using SDG.Unturned;
using System;
using uMod.Common;
using uMod.Configuration;
using uMod.Plugins;
using uMod.Plugins.Decorators;

namespace uMod.Game.Unturned
{
    /// <summary>
    /// The core Unturned plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class Unturned : Plugin
    {
        #region Initialization

        internal static readonly UnturnedProvider Universal = UnturnedProvider.Instance;

        /// <summary>
        /// Initializes a new instance of the Unturned class
        /// </summary>
        public Unturned()
        {
            // Set plugin info attributes
            Title = "Unturned";
            Author = UnturnedExtension.AssemblyAuthors;
            Version = UnturnedExtension.AssemblyVersion;
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();
        }

        #endregion Initialization

        #region Player Hooks

        /// <summary>
        /// Called when the player has connected
        /// </summary>
        /// <param name="steamPlayer"></param>
        [Hook("OnPlayerConnected")]
        private void OnPlayerConnected(SteamPlayer steamPlayer)
        {
            string playerId = steamPlayer.playerID.steamID.ToString();

            // Add player to default groups
            GroupProvider defaultGroups = Interface.uMod.Auth.Configuration.Groups;
            if (!permission.UserHasGroup(playerId, defaultGroups.Players))
            {
                permission.AddUserGroup(playerId, defaultGroups.Players);
            }
            if (steamPlayer.isAdmin && !permission.UserHasGroup(playerId, defaultGroups.Administrators))
            {
                permission.AddUserGroup(playerId, defaultGroups.Administrators);
            }

            Players.PlayerJoin(playerId, steamPlayer.playerID.playerName); // TODO: Move to OnPlayerApprove hook once available
            Players.PlayerConnected(playerId, steamPlayer);

            IPlayer player = Players.FindPlayerById(playerId);
            if (player != null)
            {
                // Set IPlayer object in SteamPlayer
                steamPlayer.IPlayer = player;

                // Call hook for plugins
                Interface.CallHook("OnPlayerConnected", player);
                Interface.CallDeprecatedHook("OnUserConnected", "OnPlayerConnected", new DateTime(2021, 1, 1), player);
            }
        }

        /// <summary>
        /// Called when the player has disconnected
        /// </summary>
        /// <param name="index"></param>
        [Hook("IOnPlayerDisconnected")]
        private void IOnPlayerDisconnected(byte index)
        {
            // Get SteamPlayer object
            SteamPlayer steamPlayer = SDG.Unturned.Provider.clients[index];

            IPlayer player = steamPlayer.IPlayer;
            if (player != null)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerDisconnected", player);
                Interface.CallDeprecatedHook("OnUserDisconnected", "OnPlayerDisconnected", new DateTime(2021, 1, 1), player);

                Players.PlayerDisconnected(player.Id);
            }
        }

        #endregion Player Hooks

        #region Server Hooks

        /// <summary>
        /// Called when a server command was run
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        [Hook("IOnServerCommand")]
        private object IOnServerCommand(string playerId, string command, string[] args)
        {
            IPlayer player = Players.FindPlayerById(playerId);
            if (player != null)
            {
                // Is the player command blocked?
                if (Universal.CommandSystem.HandleConsoleMessage(player, $"{command} {string.Join(" ", args)}") == CommandState.Completed)
                {
                    return true;
                }

                return null;
            }

            // Is the server command blocked?
            if (Interface.CallHook("OnServerCommand", command, args) != null)
            {
                return true;
            }

            return null;
        }

        #endregion Server Hooks
    }
}
