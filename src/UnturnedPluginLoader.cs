﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.Unturned
{
    /// <summary>
    /// Responsible for loading the core plugin
    /// </summary>
    public class UnturnedPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(Unturned) };

        public UnturnedPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}
