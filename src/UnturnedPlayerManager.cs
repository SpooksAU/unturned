﻿using uMod.Auth;
using uMod.Common;

namespace uMod.Game.Unturned
{
    /// <summary>
    /// Represents a Unturned player manager
    /// </summary>
    public class UnturnedPlayerManager : PlayerManager<UnturnedPlayer>
    {
        /// <summary>
        /// Create a new instance of the UnturnedPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public UnturnedPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, UnturnedPlayer player)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, player))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, IPlayer player)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, player))
            {
                return true;
            }

            return false;
        }
    }
}
