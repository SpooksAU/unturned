﻿using System.Collections.Generic;
using System.Linq;
using uMod.Command;
using uMod.Common;
using uMod.Common.Command;

namespace uMod.Game.Unturned
{
    /// <summary>
    /// Represents a binding to a generic command system
    /// </summary>
    public class UnturnedCommands : ICommandSystem
    {
        #region Initialization

        // Command handler
        internal readonly ICommandHandler CommandHandler;

        // Default universal commands
        internal readonly Commands DefaultCommands;

        // All registered commands
        internal IDictionary<string, RegisteredCommand> RegisteredCommands;

        // Registered commands
        internal class RegisteredCommand
        {
            /// <summary>
            /// The plugin that handles the command
            /// </summary>
            public readonly IPlugin Source;

            /// <summary>
            /// The name of the command
            /// </summary>
            public readonly string Command;

            /// <summary>
            /// The callback
            /// </summary>
            public readonly CommandCallback Callback;

            /// <summary>
            /// Initializes a new instance of the RegisteredCommand class
            /// </summary>
            /// <param name="source"></param>
            /// <param name="command"></param>
            /// <param name="callback"></param>
            public RegisteredCommand(IPlugin source, string command, CommandCallback callback)
            {
                Source = source;
                Command = command;
                Callback = callback;
            }
        }

        /// <summary>
        /// Initializes the command system provider
        /// </summary>
        /// <param name="commandHandler"></param>
        public UnturnedCommands(ICommandHandler commandHandler)
        {
            RegisteredCommands = new Dictionary<string, RegisteredCommand>();
            CommandHandler = commandHandler;
            CommandHandler.Callback = CommandCallback;
            CommandHandler.CommandFilter = RegisteredCommands.ContainsKey;
            //ConsolePlayer = new UnturnedConsolePlayer(); // TODO: Implement
            DefaultCommands = new Commands();
        }

        public CommandState CommandCallback(IPlayer caller, string command, string fullCommand, object[] context = null, ICommandInfo commandInfo = null)
        {
            if (!RegisteredCommands.TryGetValue(command, out RegisteredCommand registeredCommand))
            {
                return CommandState.Unrecognized;
            }

            return registeredCommand.Callback(caller, command, fullCommand, context, commandInfo);
        }

        #endregion Initialization

        #region Command Registration

        /// <summary>
        /// Registers the specified command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        public void RegisterCommand(string command, IPlugin plugin, CommandCallback callback)
        {
            // Convert the command to lowercase and remove whitespace
            command = command.ToLowerInvariant().Trim();

            // Check if the command can be overridden
            if (!CanOverrideCommand(command))
            {
                throw new CommandAlreadyExistsException(command);
            }

            // Set up a new command
            RegisteredCommand newCommand = new RegisteredCommand(plugin, command, callback);

            // Check if the command already exists in another plugin
            if (RegisteredCommands.TryGetValue(command, out RegisteredCommand cmd))
            {
                string newPluginName = plugin?.Name ?? "An unknown plugin"; // TODO: Localization
                string previousPluginName = cmd.Source?.Name ?? "an unknown plugin"; // TODO: Localization
                Interface.uMod.LogWarning($"{newPluginName} has replaced the '{command}' command previously registered by {previousPluginName}"); // TODO: Localization
            }

            // Register the command
            RegisteredCommands[command] = newCommand;
        }

        /// <summary>
        /// Unregisters the specified command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        public void UnregisterCommand(string command, IPlugin plugin) => RegisteredCommands.Remove(command);

        #endregion Command Registration

        #region Command Handling

        /// <summary>
        /// Checks if a command can be overridden
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private bool CanOverrideCommand(string command)
        {
            if (!RegisteredCommands.TryGetValue(command, out RegisteredCommand cmd) || !cmd.Source.IsCorePlugin)
            {
                return !UnturnedExtension.RestrictedCommands.Contains(command);
            }

            return true;
        }

        /// <summary>
        /// Handles a chat command message
        /// </summary>
        /// <param name="player"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public CommandState HandleChatMessage(IPlayer player, string message) => CommandHandler.HandleChatMessage(player, message);

        /// <summary>
        /// Handles a console command message
        /// </summary>
        /// <param name="player"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public CommandState HandleConsoleMessage(IPlayer player, string message, object[] context = null) => CommandHandler.HandleConsoleMessage(player, message, context);

        #endregion Command Handling
    }
}
