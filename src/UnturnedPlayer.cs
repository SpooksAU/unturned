﻿using SDG.Unturned;
using Steamworks;
using System;
using System.Globalization;
using System.Linq;
using uMod.Auth;
using uMod.Common;
using uMod.Text;
using uMod.Unity;
using UnityEngine;
using Provider = SDG.Unturned.Provider;

namespace uMod.Game.Unturned
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class UnturnedPlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        private readonly SteamPlayer steamPlayer;
        private readonly CSteamID cSteamId;

        public UnturnedPlayer(string playerId, string playerName)
        {
            // Store player details
            Id = playerId;
            Name = playerName.Sanitize();
        }

        public UnturnedPlayer(ulong playerId, string playerName)
        {
            // Store player details
            Id = playerId.ToString();
            cSteamId = new CSteamID(playerId);
        }

        public UnturnedPlayer(SteamPlayer steamPlayer) : this(steamPlayer.playerID.steamID.m_SteamID, steamPlayer.playerID.playerName)
        {
            // Store player object(s)
            this.steamPlayer = steamPlayer;
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets the object that backs the player
        /// </summary>
        public object Object => steamPlayer;

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the language for the player
        /// </summary>
        public CultureInfo Language
        {
            get
            {
                CultureInfo language = CultureInfo.GetCultures(CultureTypes.AllCultures).First(x => x.EnglishName.StartsWith(steamPlayer.language, StringComparison.InvariantCultureIgnoreCase));
                return language ?? CultureInfo.GetCultureInfo("en");
            }
        }

        /// <summary>
        /// Gets the IP address for the player
        /// </summary>
        public string Address
        {
            get
            {
                SteamGameServerNetworking.GetP2PSessionState(cSteamId, out P2PSessionState_t sessionState);
                return Parser.getIPFromUInt32(sessionState.m_nRemoteIP) ?? string.Empty;
            }
        }

        /// <summary>
        /// Gets the average network ping for the player
        /// </summary>
        public int Ping => Convert.ToInt32(steamPlayer.ping);

        /// <summary>
        /// Returns if the player is a server admin
        /// </summary>
        public override bool IsAdmin => steamPlayer != null && steamPlayer.isAdmin
            || BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Administrators);

        /// <summary>
        /// Returns if the player is a server moderator
        /// </summary>
        public override bool IsModerator => IsAdmin || BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Moderators);

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => SteamBlacklist.checkBanned(cSteamId, Parser.getUInt32FromIP(Address), out SteamBlacklistID _);

        /// <summary>
        /// Returns if the player is connected
        /// </summary>
        public bool IsConnected
        {
            get
            {
                if (steamPlayer != null)
                {
                    return Time.realtimeSinceStartup - steamPlayer.timeLastPacketWasReceivedFromClient < Provider.CLIENT_TIMEOUT;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => !IsDead;

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => steamPlayer?.player.life.isDead ?? false;

        /// <summary>
        /// Returns if the player is sleeping
        /// </summary>
        public bool IsSleeping => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Returns if the player is the server
        /// </summary>
        public bool IsServer => false;

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned)
            {
                // Ban and kick player
                Provider.ban(cSteamId, reason, (uint)duration.TotalSeconds);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining
        {
            get
            {
                SteamBlacklistID playerId = SteamBlacklist.list.First(x => x.playerID.ToString() == Id);
                return TimeSpan.FromSeconds(playerId.duration);
            }
        }

        /// <summary>
        /// Kicks the player from the game
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
            if (IsConnected)
            {
                Provider.kick(cSteamId, reason);
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                SteamBlacklist.unban(cSteamId);
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get => steamPlayer?.player?.life?.health ?? 0f;
            set => steamPlayer?.player?.life?.tellHealth(cSteamId, (byte)value);
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get
            {
                return 100f; // TODO: Implement when possible
            }
            set
            {
                throw new NotImplementedException(); // TODO: Implement when possible
            }
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount) => steamPlayer?.player?.life?.askHeal((byte)amount, true, true);

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount)
        {
            steamPlayer?.player?.life?.askDamage((byte)amount, Vector3.up * amount, EDeathCause.KILL, ELimb.SKULL, CSteamID.Nil, out EPlayerKill _);
        }

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill() => Hurt(101f);

        /// <summary>
        /// Renames the player to specified name
        /// <param name="newName"></param>
        /// </summary>
        public void Rename(string newName)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn()
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position()
        {
            return (steamPlayer?.player?.transform?.position ?? Vector3.zero).ToPosition();
        }

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            if (IsConnected && steamPlayer?.player?.transform != null)
            {
                float angle = steamPlayer.player.transform.rotation.eulerAngles.y;
                steamPlayer.player.sendTeleport(new Vector3(x, y, z), MeasurementTool.angleToByte(angle));
            }
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);
                ChatManager.say(cSteamId, prefix != null ? $"{prefix} {message}" : message, Color.white, EChatMode.LOCAL);
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Message(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command) && IsConnected)
            {
                Commander.execute(cSteamId, $"{command} {string.Join(" ", Array.ConvertAll(args, x => x.ToString()))}");
            }
        }

        #endregion Chat and Commands
    }
}
