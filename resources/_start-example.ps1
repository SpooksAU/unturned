Clear-Host

$executable = "Unturned.exe"
$installDir = Split-Path $PSCommandPath -Parent
$logFile = "$installDir\Unturned_Data\output_log.txt"
$args = '-batchmode -nographics -logfile $logFile -bind 0.0.0.0 -port 27015 -maxplayers 10 -map PEI -name "My uMod Server" -welcome "Welcome to my uMod server!" -secureserver "uMod"'

do {
    $process = Get-Process | Where-Object { $_.Path -Like "*$executable*" } -ErrorAction SilentlyContinue
    if ($process) {
        Write-Host "Stopping existing server(s)...`n"
        $process | Stop-Process -Force; Start-Sleep 5
    }

    Write-Host "Starting server...`n"
    $serverJob = Start-Job -ScriptBlock { Start-Process "$using:installDir\$using:executable" -ArgumentList $using:args -Wait }

    $logJob = Start-Job -ScriptBlock { Get-Content "$using:logFile" -Wait }
    while ($serverJob.State -eq 'Running' -And $logJob.HasMoreData) {
      Receive-Job $logJob
      Start-Sleep -Milliseconds 200
    }

    Receive-Job $logJob; Stop-Job $logJob; Remove-Job $logJob; Remove-Job $serverJob -Force
    Write-Host "Restarting server...`n"; Start-Sleep 5
} while (!($serverJob.State -eq 'Running'))
